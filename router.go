package corewebframe

import (
	"net/http"
)

//Router ...
type Router interface {
	http.Handler
	Routes

	MethodFunc(method string, pattern string, h http.HandlerFunc)
	Options(pattern string, h http.HandlerFunc)
	Post(pattern string, h http.HandlerFunc)
	Get(pattern string, h http.HandlerFunc)
	Put(pattern string, h http.HandlerFunc)
	Del(pattern string, h http.HandlerFunc)
	All(pattern string, h http.HandlerFunc)
}

//Routes ...
type Routes interface {
	RouteList() []Route
}
