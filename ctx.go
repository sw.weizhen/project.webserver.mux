package corewebframe

import (
	"context"
	"fmt"
	"net/http"
	"time"
)

//RouteCtxKey ...
var RouteCtxKey = &contextKey{"RouteContext"}

var _ context.Context = (*directContext)(nil)

type directContext Context

//Context ...
type Context struct {
	Routes           Routes
	URLParams        RouteParams
	routeParams      RouteParams
	RoutePatterns    []string
	RoutePath        string
	RouteMethod      string
	routePattern     string
	methodNotAllowed bool

	parentCtx context.Context
}

//RouteParams ...
type RouteParams struct {
	Keys   []string
	Values []string
}

type contextKey struct {
	name string
}

//URLReqParam ...
func URLReqParam(r *http.Request, key string) string {
	if rctx := routeContext(r.Context()); rctx != nil {
		return rctx.urlParam(key)
	}

	return ""
}

//URLReqParamMap ...
func URLReqParamMap(r *http.Request) map[string]string {
	if rctx := routeContext(r.Context()); rctx != nil {
		return rctx.urlParamMap()
	}

	return make(map[string]string)
}

func routeContext(ctx context.Context) *Context {
	val, _ := ctx.Value(RouteCtxKey).(*Context)
	return val
}

//NewRouteContext ...
func newRouteContext() *Context {
	return &Context{}
}

//CtxClear ...
func (ctx *Context) CtxClear() {
	ctx.Routes = nil
	ctx.RoutePath = ""
	ctx.RouteMethod = ""
	ctx.RoutePatterns = ctx.RoutePatterns[:0]
	ctx.URLParams.Keys = ctx.URLParams.Keys[:0]
	ctx.URLParams.Values = ctx.URLParams.Values[:0]

	ctx.routePattern = ""
	ctx.routeParams.Keys = ctx.routeParams.Keys[:0]
	ctx.routeParams.Values = ctx.routeParams.Values[:0]
	ctx.methodNotAllowed = false
	ctx.parentCtx = nil
}

func (ctx *Context) urlParamMap() map[string]string {
	rtnMap := make(map[string]string)

	for i := 0; i < len(ctx.URLParams.Keys); i++ {
		rtnMap[ctx.URLParams.Keys[i]] = ctx.URLParams.Values[i]
	}

	return rtnMap
}

//urlParam ...
func (ctx *Context) urlParam(key string) string {
	for k := len(ctx.URLParams.Keys) - 1; k >= 0; k-- {
		if ctx.URLParams.Keys[k] == key {
			return ctx.URLParams.Values[k]
		}
	}
	return ""
}

//Add ...
func (s *RouteParams) Add(key, value string) {
	s.Keys = append(s.Keys, key)
	s.Values = append(s.Values, value)
}

//Deadline ...
func (d *directContext) Deadline() (deadline time.Time, ok bool) {
	return d.parentCtx.Deadline()
}

//Done ...
func (d *directContext) Done() <-chan struct{} {
	fmt.Println("ctx done")
	return d.parentCtx.Done()
}

//Err ...
func (d *directContext) Err() error {
	return d.parentCtx.Err()
}

//Value ...
func (d *directContext) Value(key interface{}) interface{} {
	if key == RouteCtxKey {
		return (*Context)(d)
	}
	return d.parentCtx.Value(key)
}
